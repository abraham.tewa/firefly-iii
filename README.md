# Firefly III docker installation

To install, simply run:

```bash
docker compose up -d
```

Instances will be accessible at:
* Firefly III App: http://localhost:2000
* Firefly III Importer: http://localhost:2001